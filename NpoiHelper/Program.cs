﻿using MySample.Npoi;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;

namespace MySample
{
    class Program
    {
        class Sample
        {
            public string Id { get; set; }   //故意在這裡定義為string 但輸出到Excel中需轉為數字
            public string Name { get; set; }
            public DateTime Birthday { get; set; }
        }

        static void Main(string[] args)
        {
            var data = new List<Sample>
            {
                new Sample { Id = "1", Name = "A", Birthday = DateTime.Parse("2001/1/1 12:00") },
                new Sample { Id = "2", Name = "B", Birthday = DateTime.Parse("2001/1/2 12:00") },
                new Sample { Id = "3", Name = "C", Birthday = DateTime.Parse("2001/1/3 12:00") },
            };

            //無範本 ( 直接產新 Excel )
            var param1 = NoTemplate(data);
            NpoiHelper.ExportExcel(param1);

            //有範本 ( 根據現有範本塞資料進去 )
            var param2 = HasTemplate(data);
            NpoiHelper.ExportExcel(param2);
        }

        private static NpoiParam<Sample> NoTemplate(List<Sample> data)
        {
            var param = new NpoiParam<Sample>
            {
                Workbook = new XSSFWorkbook(),            //要用一個新的或是用你自己的範本
                Data = data,                              //資料
                FileFullName = @"D:\result1.xlsx",        //Excel檔要存在哪
                SheetName = "No Template",                //Sheet要叫什麼名子
                ColumnMapping = new List<ColumnMapping>   //欄位對應 (處理Excel欄名、格式轉換)
                {
                    new ColumnMapping { ModelFieldName = "Id", ExcelColumnName = "流水號", DataType = NpoiDataType.Number, Format = "0.00"},
                    new ColumnMapping { ModelFieldName = "Name", ExcelColumnName = "名子", DataType = NpoiDataType.String},
                    new ColumnMapping { ModelFieldName = "Birthday", ExcelColumnName = "生日", DataType = NpoiDataType.Date, Format="yyyy-MM-dd"},
                },
                FontStyle = new FontStyle                 //是否需自定Excel字型大小
                {
                    FontName = "Calibri",
                    FontHeightInPoints = 11,
                },
                ShowHeader = true,                        //是否畫表頭
                IsAutoFit = true,                         //是否啟用自動欄寬
            };

            return param;
        }

        private static NpoiParam<Sample> HasTemplate(List<Sample> data)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory + "template.xlsx";

            var param = new NpoiParam<Sample>
            {
                Workbook = new XSSFWorkbook(path),        //要用一個新的或是用你自己的範本
                Data = data,                              //資料
                FileFullName = @"D:\result2.xlsx",        //Excel檔要存在哪
                SheetName = "Existing Template",          //Sheet要叫什麼名子
                ColumnMapping = new List<ColumnMapping>   //欄位對應 (處理Excel欄名、格式轉換)
                {
                    new ColumnMapping { ModelFieldName = "Id", ExcelColumnName = "流水號", DataType = NpoiDataType.Number, Format = "0.00"},
                    new ColumnMapping { ModelFieldName = "Name", ExcelColumnName = "名子", DataType = NpoiDataType.String},
                    new ColumnMapping { ModelFieldName = "Birthday", ExcelColumnName = "生日", DataType = NpoiDataType.Date, Format="yyyy-MM-dd"},
                },
                ShowHeader = false,                       //是否畫表頭
                ColumnStartFrom = 2,
                RowStartFrom = 1,
            };

            return param;
        }
    }
}
