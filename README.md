# 不自訂範本 #
假設我希望Excel的結果長這樣<br />
![無範本](https://az787680.vo.msecnd.net/user/jesper/ef549709-956a-4228-9c87-75c2f223e86d/1521768809_13957.png "無範本")<br />
<br />
呼叫的程式碼會長得像
<pre><code>
//組裝參數並且出Excel檔
var param = GetParam(data);
NpoiHelper.ExportExcel(param);

private static NpoiParam<Sample> GetParam(List<Sample> data)
{
    var param = new NpoiParam<Sample>
    {
        Workbook = new XSSFWorkbook(),            //要用一個新的或是用你自己的範本
        Data = data,                              //資料
        FileFullName = @"D:\result.xlsx",         //Excel檔要存在哪
        SheetName = "Data",                       //Sheet要叫什麼名子
        ColumnMapping = new List<ColumnMapping>   //欄位對應 (處理Excel欄名、格式轉換)
        {
            new ColumnMapping { ModelFieldName = "Id", ExcelColumnName = "流水號", DataType = NpoiDataType.Number, Format = "0.00"},
            new ColumnMapping { ModelFieldName = "Name", ExcelColumnName = "名子", DataType = NpoiDataType.String},
            new ColumnMapping { ModelFieldName = "Birthday", ExcelColumnName = "生日", DataType = NpoiDataType.Date, Format="yyyy-MM-dd"},
        },
        FontStyle = new FontStyle                 //是否需自定Excel字型大小
        {
            FontName = "Calibri",
            FontHeightInPoints = 11,
        },
        ShowHeader = true,                        //是否畫表頭
        IsAutoFit = true,                         //是否啟用自動欄寬
    };

    return param;
}
</code></pre>
<br />
<br />
# 要自訂範本 #
假設我希望Excel的結果長這樣<br />
![有範本](https://az787680.vo.msecnd.net/user/jesper/ef549709-956a-4228-9c87-75c2f223e86d/1521769678_83039.png "有範本")<br />
<br />
呼叫的程式碼會長得像
<pre><code>
//組裝參數並且出Excel檔
var param = GetParam(data);
NpoiHelper.ExportExcel(param);

private static NpoiParam<Sample> GetParam(List<Sample> data)
{
    var param = new NpoiParam<Sample>
    {
        Workbook = new XSSFWorkbook(@"D:\template.xlsx"),   //要用一個新的或是用你自己的範本
        Data = data,                                        //資料
        FileFullName = @"D:\result.xlsx",                   //Excel檔要存在哪
        SheetName = "Data",                                 //Sheet要叫什麼名子
        ColumnMapping = new List<ColumnMapping>             //欄位對應 (處理Excel欄名、格式轉換)
        {
            new ColumnMapping { ModelFieldName = "Id", ExcelColumnName = "流水號", DataType = NpoiDataType.Number, Format = "0.00"},
            new ColumnMapping { ModelFieldName = "Name", ExcelColumnName = "名子", DataType = NpoiDataType.String},
            new ColumnMapping { ModelFieldName = "Birthday", ExcelColumnName = "生日", DataType = NpoiDataType.Date, Format="yyyy-MM-dd"},
        },
        ShowHeader = false,                                 //是否畫表頭
        ColumnStartFrom = 2,
        RowStartFrom = 1,
    };

    return param;
}
</code></pre>